scannerHome = "E:/sonar-scanner-4.0.0.1744-windows/bin/sonar-scanner.bat"
HOST = "http://localhost:9000/"

def scan(String propertiesFile, String projectRoot)
{
  
    def scanstatus
    // requires SonarQube Scanner 2.8+
    //def scannerHome = tool 'SonarScanner';
    
	withSonarQubeEnv('Sonar') 
    {
        
         scanstatus = bat returnStatus: true, script: "${scannerHome} -Dsonar.projectBaseDir=\"${projectRoot}\" -Dproject.settings=${propertiesFile} -Dsonar.host.url=${HOST} -Dsonar.scm.exclusions.disabled=true"
         
         bat "echo 'Scan status is: ${scanstatus}'"
    }
      	
          if(scanstatus==0)
           {  
             timeout(time: 1, unit: 'MINUTES') 
             { // Just in case something goes wrong, pipeline will be killed after a timeout
              
             
                // Parameter indicates whether to set pipeline to UNSTABLE if Quality Gate fails
                // true = set pipeline to UNSTABLE, false = don't
                 // Requires SonarQube Scanner for Jenkins 2.7+
                 def qg = waitForQualityGate(abortPipeline: true) 
          
                 bat "echo 'QualityGate status is: ${qg.status}'" 
              
                if (qg.status == 'ERROR')
                 {
                    currentBuild.result = 'FAILURE'
                    error "Pipeline aborted due to quality gate failure: ${qg.status}"
                 }
                else
                {
                   currentBuild.result = 'SUCCESS'
                }
             }    
                
             
            }
            else
            { 
              currentBuild.result = 'FAILURE'
            }

      bat "echo 'currentBuild.result is: ${currentBuild.result}'" 
       
      return currentBuild.result 
   
}

return this
